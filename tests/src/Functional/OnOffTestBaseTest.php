<?php

namespace Drupal\Tests\testtools\Functional;

use Drupal\testtools\OnOffTestBase;

/**
 * Tests OnOffTestBase.
 */
class OnOffTestBaseTest extends OnOffTestBase {

  /**
   * {@inheritdoc}
   */
  protected function getModuleName(): string {
    return 'node';
  }

}
