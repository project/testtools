<?php

namespace Drupal\testtools;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that a given module can be enabled and disabled.
 *
 * @group testtools
 */
abstract class OnOffTestBase extends BrowserTestBase {

  /**
   * Returns the name of the module to test.
   *
   * @return string
   *   Name of the module.
   */
  abstract protected function getModuleName(): string;

  /**
   * Main test function.
   */
  final public function testOnOff(): void {
    $moduleName = $this->getModuleName();

    $this->onoff($moduleName);
    $this->onoff($moduleName);
  }

  /**
   * Enables and disables a module.
   *
   * @param string $moduleName
   *   Name of the module.
   */
  private function onoff(string $moduleName): void {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $installer */
    $installer = \Drupal::service('module_installer');

    $installer->install([$moduleName]);
    $this->afterInstallAndUninstall();

    $installer->uninstall([$moduleName]);
    $this->afterInstallAndUninstall();
  }

  /**
   * Operations to run after install or uninstall.
   */
  protected function afterInstallAndUninstall(): void {
    $this->container = \Drupal::getContainer();
    $this->container->get('router.builder')->rebuildIfNeeded();
  }

}
